import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APIRequestFactory

from posts.models import Post


@pytest.fixture
def post_factory():
    post = Post()
    post.name = "Post a"
    post.content = "Content Post a"
    post.email = "user_a@gmail.com"
    post.save()

    return post


@pytest.fixture
def factory_factory():
    return APIRequestFactory()


@pytest.fixture
def client_factory():
    return APIClient()


@pytest.mark.django_db
def test_add_post_successfully(factory_factory, client_factory):
    """
    Tests add post.
    """
    url = reverse("add_post")
    data = {
        "name": "Post a",
        "content": "Content Post a",
        "email": "user_b@gmail.com"
    }
    response = client_factory.post(url, data, format="json")

    posts = Post.objects.filter(name="Post a")

    assert response.status_code == status.HTTP_201_CREATED
    assert posts[0].name == "Post a"
    assert len(posts) == 1


@pytest.mark.django_db
def test_add_post_with_missing_parameters(factory_factory, client_factory):
    """
    Tests add post with missing parameters.
    Given: missing content post
    """
    url = reverse("add_post")
    data = {
        "name": "Post a",
        "email": "user_b@gmail.com"
    }
    response = client_factory.post(url, data, format="json")
    aux = response.json()

    assert response.status_code, status.HTTP_406_NOT_ACCEPTABLE
    assert aux.get("error"), "Required parameters are missing"


@pytest.mark.django_db
def test_add_post_with_name_already_exists(
        factory_factory, client_factory, post_factory
):
    """
    Tests add post with name already exists.
    Given: Post with the same name.
    """
    url = reverse("add_post")
    data = {
        "name": "Post a",
        "content": "Content Post a",
        "email": "user_b@gmail.com",
    }

    response = client_factory.post(url, data, format="json")
    aux = response.json()

    assert response.status_code, status.HTTP_400_BAD_REQUEST
    assert aux.get("error"), "Post already exists"


@pytest.mark.django_db
def test_add_post_with_invalid_email(factory_factory, client_factory):
    """
    Tests add post with invalid email.
    Given: Invalid email.
    """
    url = reverse("add_post")
    data = {
        "name": "Post a",
        "content": "Content Post a",
        "email": "user_c",
    }
    response = client_factory.post(url, data, format="json")
    aux = response.json()

    assert response.status_code, status.HTTP_406_NOT_ACCEPTABLE
    assert aux.get("error"), "Invalid email"


@pytest.mark.django_db
def test_get_posts(factory_factory, client_factory, post_factory):
    """
    Tests get paginated posts.
    """
    url = reverse("get_posts",  kwargs={"last_id": 0})
    response = client_factory.get(url)

    assert response.status_code, status.HTTP_200_OK


@pytest.mark.django_db
def test_like_post(factory_factory, client_factory, post_factory):
    """
    Tests like post.
    """
    post = post_factory
    url = reverse("like_post", kwargs={"post_id": post.pk})
    response = client_factory.put(url, format="json")
    post.refresh_from_db()

    assert response.status_code, status.HTTP_200_OK
    assert post.likes, 1


@pytest.mark.django_db
def test_dislike_post(factory_factory, client_factory, post_factory):
    """
    Tests dislike post.
    """
    post = post_factory
    url = reverse("dislike_post", kwargs={"post_id": post.pk})
    response = client_factory.put(url, format="json")
    post.refresh_from_db()

    assert response.status_code, status.HTTP_200_OK
    assert post.dislikes, 1
