import pytest

from posts.serializers import PostSerializer


@pytest.mark.django_db
def test_valid_post_serializer():
    """
    Tests if serializer at works as expected.
    """
    valid_serializer_data = {
        "name": "Post 1",
        "content": "Content Post 1",
        "email": "user_a@gmail.com",
        "likes": 1,
        "dislikes": 1
    }
    serializer = PostSerializer(data=valid_serializer_data)
    assert serializer.is_valid()
    assert serializer.validated_data == valid_serializer_data
    assert serializer.data == valid_serializer_data
    assert serializer.errors == {}


@pytest.mark.django_db
def test_invalid_post_serializer():
    """
    Tests if serializer at works as expected.
    """
    invalid_serializer_data = {
        "name": "Post 1",
        "content": "Content Post 1"
    }
    serializer = PostSerializer(data=invalid_serializer_data)
    assert not serializer.is_valid()
    assert serializer.validated_data == {}
    assert serializer.data == invalid_serializer_data
    assert serializer.errors == {"email": ["This field is required."]}
