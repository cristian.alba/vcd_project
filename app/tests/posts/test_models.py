import pytest

from posts.models import Post


@pytest.mark.django_db
def test_post_model():
    """
    Tests if model at works as expected.
    """
    post = Post(
        name="Post 1", content="Content Post 1", email="user_a@gmail.com"
    )
    post.save()
    assert post.name == "Post 1"
    assert post.content == "Content Post 1"
    assert post.email == "user_a@gmail.com"
    assert post.created_at
    assert str(post) == post.name
